import time
import sys
import pygame   
import serial
import socket

class button():
    def __init__(self, color, x,y,width,height, text=''):
        self.color = color
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.text = text
        self.name = text

    def draw(self,win,outline=None):
        #Call this method to draw the button on the screen
        if outline:
            pygame.draw.rect(win, outline, (self.x-2,self.y-2,self.width+4,self.height+4),0)
            
        pygame.draw.rect(win, self.color, (self.x,self.y,self.width,self.height),0)
        
        if self.text != '':
            font = pygame.font.SysFont('comicsans', 60)
            text = font.render(self.text, 1, (0,0,0))
            win.blit(text, (self.x + (self.width/2 - text.get_width()/2), self.y + (self.height/2 - text.get_height()/2)))

    def isOver(self, pos):
        #Pos is the mouse position or a tuple of (x,y) coordinates
        if pos[0] > self.x and pos[0] < self.x + self.width:
            if pos[1] > self.y and pos[1] < self.y + self.height:
                return True
            
        return False

    def getVoltage(self, pos):
        #Pos is the mouse position or a tuple of (x,y) coordinates
        ratio = (pos[0] - self.x )* 1024 / self.width
        self.text = '{} : {}'.format(self.name,int(ratio))
        return int(ratio)

class scket:
    def __init__(self,address = 'localhost',port = 10000):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
        server_address = (address, port)
        print  ('connecting to %s port %s' % server_address)
        self.sock.connect(server_address)
    
    def write(self,data):
        self.sock.sendall(data)

    def __del__(self):
        self.sock.close()

def main():
    pygame.init()
    ser = scket('192.168.50.109',10000) 
    #serial.Serial(serAddress) 
    screen = pygame.display.set_mode((800,450))
    screen.fill((150,150,150))
    leftButton = button((0,255,0),0,0,400,250,"LeftButton")
    rightButton = button((0,255,0),400,0,400,250,"RightButton")
    voltageButton = button((255,255,0),0,250,800,100,"Left")
    voltageButton2 = button((255,255,0),0,350,800,100,"Right")
    running = True
    a = 0
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                if leftButton.isOver(pos):
                    leftButton.color = (255,0,0)
                    ser.write(b'TCM-5-0\n')
                    a += 1
                if rightButton.isOver(pos):
                    rightButton.color = (255,0,0)
                    ser.write(b'TCM-6-0\n')
                    a += 1 
                if voltageButton.isOver(pos):
                    #voltageButton.getVoltage(pos)
                    reult = 'TCM-3-{}a\n'.format(voltageButton.getVoltage(pos))
                    ser.write(bytes(reult,'ascii')) 
                if voltageButton2.isOver(pos):
                    #voltageButton.getVoltage(pos)
                    reult = 'TCM-4-{}a\n'.format(voltageButton2.getVoltage(pos))
                    ser.write(bytes(reult,'ascii')) 
            if event.type == pygame.MOUSEBUTTONUP:
                rightButton.color = (0,255,0)
                leftButton.color = (0,255,0)
                ser.write(b'TCM-6-1\n') 
                ser.write(b'TCM-5-1\n')
                a += 2 
        #window redraw
        screen.fill((150,150,150))
        leftButton.draw(screen,(0,0,0))
        rightButton.draw(screen,(0,0,0))
        voltageButton.draw(screen,(0,0,0))
        voltageButton2.draw(screen,(0,0,0))
        pygame.display.update()
    exit()
    

if __name__ == "__main__":
    main()